import org.junit.jupiter.api.Test;

public class EnrollmentsAdministrationTest {
    @Test
    void shouldBeAbleToAddStudents() {
        EnrollmentsAdministration enrollmentsAdministration = new EnrollmentsAdministration();
        enrollmentsAdministration
                .addStudent(new Student("Sonal", "2"))
                .addStudent(new Student("jdx","3"));
        enrollmentsAdministration.showStudentInformation();
    }
}
