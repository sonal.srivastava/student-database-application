import org.junit.jupiter.api.Test;

public class StudentTest {
    @Test
    void shouldBeAbleToEnrollInCourse() {
        EnrollmentsAdministration enrollmentsAdministration = new EnrollmentsAdministration();
        Student student = new Student("Sonal", "2");
        enrollmentsAdministration.addStudent(student);
        student.enrollInUniversityCourse(UniversityCourse.HISTORY101);
        student.showStudentStatusInformation();
    }

    @Test
    void shouldBeAbleViewBalance() {
        EnrollmentsAdministration enrollmentsAdministration = new EnrollmentsAdministration();
        Student student = new Student("Sonal", "1");
        enrollmentsAdministration.addStudent(student);
        student.enrollInUniversityCourse(UniversityCourse.HISTORY101);
        student.viewBalance();
    }
    @Test
    void shouldBeAblePayTution() {
        EnrollmentsAdministration enrollmentsAdministration = new EnrollmentsAdministration();
        Student student = new Student("Sonal", "3");
        enrollmentsAdministration.addStudent(student);
        student.enrollInUniversityCourse(UniversityCourse.HISTORY101);
        student.payTuition(20);
        student.viewBalance();
    }
}
