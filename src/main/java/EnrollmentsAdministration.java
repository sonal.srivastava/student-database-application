import java.util.HashMap;
import org.apache.commons.lang3.RandomStringUtils;

public class EnrollmentsAdministration {
    HashMap<Integer, Student> StudentIDHashMap= new HashMap<>();

    public EnrollmentsAdministration addStudent(Student studentObj) {
        int uniqueStudentID=generateUniqueStudentID(studentObj.getYear());
        studentObj.setUniqueStudentID(uniqueStudentID);
        StudentIDHashMap.put(uniqueStudentID, studentObj);
        return this;
    }

    private int generateUniqueStudentID(String year) {
        return Integer.parseInt(year + RandomStringUtils.randomNumeric(4));
    }

    public void showStudentInformation() {
        for (Student student:
             StudentIDHashMap.values()) {
            System.out.println("***************");
            student.showStudentStatusInformation();
        }
    }
}
