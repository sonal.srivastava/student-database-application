public enum UniversityCourse {
    HISTORY101("History 101", 600),
    MATHEMATICS101("Mathematics 101", 600),
    ENGLISH101("English 101", 600),
    CHEMISTRY101("Chemistry 101", 600),
    COMPUTERSCIENCE101("Computer Science 101", 600);

    private final String courseName;
    private final double fee;

    UniversityCourse(String courseName, double fee) {
        this.courseName = courseName;
        this.fee = fee;
    }

    public String getName() {
        return courseName;
    }

    public double getFee() {
        return fee;
    }
}
