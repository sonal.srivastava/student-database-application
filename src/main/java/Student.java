import java.util.ArrayList;

public class Student {
    private final String year;
    private final String name;
    public int uniqueStudentID;
    private final ArrayList<UniversityCourse> UniversityCoursesEnrolledArrayList = new ArrayList<>();
    private double balance;

    public String getYear() {
        return year;
    }

    public Student(String name, String year) {
        this.name=name;
        this.year=year;
    }

    public void setUniqueStudentID(int uniqueStudentID) {
        this.uniqueStudentID=uniqueStudentID;
    }

    public void enrollInUniversityCourse(UniversityCourse universityCourse) {
        UniversityCoursesEnrolledArrayList.add(universityCourse);
        addUniversityCourseFeeToBalance(universityCourse.getFee());
    }

    private void addUniversityCourseFeeToBalance(double fee) {
        balance+=fee;
    }

    public void showStudentStatusInformation() {
        System.out.print("Name : "+name+"\nID : "+uniqueStudentID+"\nCourses Enrolled : ");
        for (UniversityCourse course:
             UniversityCoursesEnrolledArrayList) {
            System.out.print("| "+ course.getName()+" |");
        }
        System.out.printf("\nBalance : %s%n", balance);
    }

    public void viewBalance() {
        System.out.println("You have following balance due to be paid for courses enrolled : "+balance);
    }

    public void payTuition(double amount) {
        balance-=amount;
        System.out.println("Following amount successfully paid : "+amount);
    }
}
